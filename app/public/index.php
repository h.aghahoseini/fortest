<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>


    <link rel="stylesheet" href="./style.css">

</head>

<body>



    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">family</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody id="tableBody">

        </tbody>
    </table>



    <div class="container">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Add New
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form name="add" action="" method="post" class="add">


                            <div class="mb-3">
                                <label for="name_input" class="form-label">name</label>
                                <input type="text" class="form-control" id="name_input" required minlength="3" ">
                                <div id=" name_help" class="form-text text-danger helper">
                            </div>
                    </div>

                    <div class="mb-3">
                        <label for="family_input" class="form-label">family</label>
                        <input type="text" class="form-control" id="family_input" required minlength="3">
                        <div id="family_help" class="form-text text-danger helper"></div>

                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="send_request">Send Request</button>
                    </div>

                </div>
            </div>
        </div>







        <script>


            function addRow($id, $name, $family) {
                // Get the table body element in which you want to add row
                let table = document.getElementById("tableBody");

                // Create row element
                let row = document.createElement("tr")

                // Create cells
                let c1 = document.createElement("td")
                let c2 = document.createElement("td")
                let c3 = document.createElement("td")
                let c4 = document.createElement("td")

                // Insert data to cells
                c1.innerText = $id
                c2.innerText = $name
                c3.innerText = $family

                // Append cells to row
                row.appendChild(c1);
                row.appendChild(c2);
                row.appendChild(c3);
                row.appendChild(c4);

                // Append row to table body
                table.appendChild(row)
            }



            function read() {

                $.ajax({
                    url: './src/api.php',
                    method: 'GET',
                    success: function (records) {
                        //console.log(records);
                        res = JSON.parse(records)
                        //console.log(res[0]);
                        //console.log(res[3][id]);


                        //console.log(res);

                        
                        res.forEach(function (item) {
                            //console.log(item.id)
                            //console.log(item)

                            addRow(item.id, item.name, item.family);
                        })
                        

                    }
                })


            }



            read();

            




            var name_input = $("#name_input").val();
            var family_input = $("#family_input").val();





            var origin = 1;
            let x = document.querySelectorAll(".form-control");

            x.forEach(function (elem) {

                elem.addEventListener("keyup", function () {
                    console.log('dsf')

                    origin = 0;

                    if (elem.value.length > 3) {

                        elem.classList.remove("invalid");
                        elem.classList.remove("vinvalid");
                        elem.classList.add("valid");
                    }
                    else {

                        elem.classList.remove("valid");
                        elem.classList.add("invalid");
                    }

                })
            });

            document.getElementById("send_request").addEventListener("click", function () {


                if (document.getElementById("name_input").value.length > 3 && document.getElementById("family_input").value.length > 3) {

                    console.log(document.getElementById("name_input").value)
                    data = { name: document.getElementById('name_input').value, family: document.getElementById("family_input").value };
                    opt = JSON.stringify(data);




                    $.ajax({
                        url: './src/api.php',
                        method: 'POST',
                        data: opt,
                        success: function (values) {
                            console.log(values);
                            
                            item = JSON.parse(values);
                            console.log(item);
                            addRow(item.id,item.name,item.family);
                            $('form[name=add]').trigger('reset');
                            $('.modal').modal('hide');
                            
                        }
                    })




                }

                if (document.getElementById("name_input").value.length < 4) {

                    $('#name_input').addClass('vinvalid')
                    $("#name_help").html("this field is required and minlenth is 3");


                }

                if (document.getElementById("family_input").value.length < 4) {

                    $('#family_input').addClass('vinvalid')
                    $("#family_help").html("this field is required and minlenth is 3");



                }

            })


        </script>


</body>

</html>