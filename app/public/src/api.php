<?php


require "../vendor/autoload.php";
use Buki\Pdox;

$config = [
    'host'      => 'mysql:3306',
    'database'  => 'testdb',
    'username'  => 'root',
    'password'  => '12345678',
];

$db = new Pdox($config);

$data = json_decode(file_get_contents("php://input"));





function create($name,$family) 
{

  global $db;
  


  $data = [
    'name' => $name,
    'family' => $family
  ];

  $query = $db->table('users')->insert($data);

  if($query)
  {    

    $records = $db->table('users')
    ->select('*')
    ->where('id', '=', $db->insertId())
    ->get();

    $res = json_encode($records);

    print_r($res);



  }   
  

}


function read()
{
    global $db;
    $records = $db->table('users')
        ->select('*')
        ->getAll();

    $res = json_encode($records); 

    print_r($res);


}


$method = $_SERVER['REQUEST_METHOD'];

switch ($method) 
{

  case 'PUT':
    update();
    break;


  case 'POST':
    create($data->name,$data->family);
    break;

  case 'GET':
    read();
    break;

  case 'Delete':  
    delete();
    break;

  default:
    echo $method;
    break;

}
